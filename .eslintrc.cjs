module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: ['jest'],
  env: {
    "jest/globals": true
  },
  rules: {
    'class-methods-use-this': ['off'],
    'import/extensions': ['off']
  },
};
