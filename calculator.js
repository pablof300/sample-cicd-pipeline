class SimpleCalculator {
  add(x, y) {
    return x + y;
  }
}

export default SimpleCalculator;
